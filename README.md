# Turn-based Combat System Readme - 180124

Overview:
This is a system design to incorporate customizable player characters into a turn-based combat system.
As a developer you can inject adaptable character stats, animations and damage mechanics into the encounter with designated teams and turn order.

A target and damage system has been included to allow easy interaction between characters on their turn. As well as a scaling system design for leveling up characters independently.


Purpose:
This is designed to allow easy inclusion of turn-based combat into an RPG type game. As a developer, you will be able to adapt the following for each character: Statistics, Leveling Up Scaling, Teams, Damage Mechanics, Animations, Models and User Interface


Usage:
The package includes pre-made charters and User Interface as reference. In order to create a new character, duplicate a previous character folder found in the Scripts file (such as “Mage”). You can then edit set stats (read below) on the object, and mechanical differences in the script. To change base statistics for all characters, edit the “CharacterStats” script found in the scripts folder.

For further editing of code, each script has been commented with descriptions of what each function does using the “//” sign.

Areas where optional Animations, Turn-based damage and other functions can be injected are marked with the “/**/” sign.


Download link to .UnityPackage:
----------> https://gitlab.com/PotatoThomas/180124_turn-based-combat-system.git