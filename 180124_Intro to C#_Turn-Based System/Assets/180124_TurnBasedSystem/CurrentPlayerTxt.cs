﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class CurrentPlayerTxt : MonoBehaviour
{

     public Text Playertxt;
     public static CurrentPlayerTxt instance;

     private void Awake() 
     {
         instance = this;
     }
    public void changePlayerName (string name)
    {

            Playertxt.text = name;  //Sets UI element to display current Player Name
    }
}
