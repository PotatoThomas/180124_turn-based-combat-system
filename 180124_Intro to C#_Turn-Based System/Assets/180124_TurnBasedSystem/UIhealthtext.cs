﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class UIhealthtext : MonoBehaviour
{

    public Text HPtxt;
    public static UIhealthtext instance;

    private void Awake()
    {
        instance = this;
    }
    public void changePlayerHP(string HP)
    {

        HPtxt.text = HP;    //Sets UI element to display current Health
    }
}

