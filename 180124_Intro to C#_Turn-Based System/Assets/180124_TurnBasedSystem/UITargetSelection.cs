﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITargetSelection : MonoBehaviour {

    Dropdown dropDown;

    List<CharacterBase> availableTargets = new List<CharacterBase> ();

    void Awake () {
        dropDown = GetComponent<Dropdown> ();
    }

    void Start () {
        SetDropDownOptions ();
    }

    void OnEnable () {
        TurnManager.OnNewTurn += SetDropDownOptions;
    }

    void OnDisable () {
        TurnManager.OnNewTurn -= SetDropDownOptions;
    }

    void SetDropDownOptions () {
        availableTargets = new List<CharacterBase> ();
        List<Dropdown.OptionData> options = new List<Dropdown.OptionData> ();
        foreach (BattleManager.TeamCharacters teamChars in BattleManager.instance.CharacterList) {
            if (teamChars != BattleManager.instance.activeTeam) {
                foreach (var item in teamChars.characters) {
                    options.Add (new Dropdown.OptionData (item.name)); //adds non-active players to list
                    availableTargets.Add (item);
                }
            }
        }
        dropDown.options = options;

        if (availableTargets.Count > 0){
            TargetSelected (0);
        }
    }

    public void TargetSelected (int index) {
        BattleManager.instance.UpdateTarget (availableTargets[index]);
    }

}