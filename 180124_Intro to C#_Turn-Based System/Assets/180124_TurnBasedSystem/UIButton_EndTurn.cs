﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButton_EndTurn : MonoBehaviour
{
    

    public void RoundDone(){
        TurnManager.instance.TurnExecute();  //End turn and start attack routine
    }
}
