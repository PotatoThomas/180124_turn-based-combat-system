﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class UIdmgText : MonoBehaviour
{

    public Text DMGtxt;
    public static UIdmgText instance;

    private void Awake()
    {
        instance = this;
    }
    public void changePlayerDMG(string DMG)
    {

        DMGtxt.text = DMG;          //Sets UI element to display current Damage
    }
}
