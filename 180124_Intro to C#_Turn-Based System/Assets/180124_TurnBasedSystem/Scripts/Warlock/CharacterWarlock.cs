﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterWarlock : CharacterBase, ICharacter {

    public CharacterObject characterStats; //Template of base stats

    public int damage;
    public GameObject target;

    public void Init () {
        base.currentStats = new CharacterStats (characterStats._characterStats);
    }

    public void MyTurn () {

        if (base.currentStats.dead == true) //when the turn beggins: If the character is dead, it is removed from the list of characters and a new turn is started
        {
            Debug.Log ("Character Dead!");
            BattleManager.instance.CharacterList[0].characters.Remove (this);
            TurnManager.instance.TurnComplete ();
            base.Die ();
        }

        //input Turn-Based damage or mechanic here <-------

        //UI elements switch to new character

        StartCoroutine (AttackRoutine ()); //otherwise they are able to attack 
    }

    public void Damage (float amount) { //when character is dammaged, it is sent here
        Debug.Log (transform.name + " receiving damage - " + amount);
        base.currentStats.baseHP -= amount;
        if (base.currentStats.baseHP <= 0) {
            Debug.Log ("Character Dead!");
            base.currentStats.dead = true;
            BattleManager.instance.CharacterList[0].characters.Remove (this);
            base.Die ();
        }
    }

    IEnumerator AttackRoutine () { //when a character attacks, their damage is determined here and sent to battle mannager

        Debug.Log (transform.name + " attacking -> " + BattleManager.instance.selectedTarget);
        (BattleManager.instance.selectedTarget as ICharacter).Damage (base.currentStats.CalculatedDamage ());
        // BattleManager.instance.DealDamage (damage, target);
        //BattleManager.instance.CharacterList[0].characters[0].GetComponent<ICharacter> ().Damage (base.currentStats.currentStrength);

        TurnManager.instance.TurnComplete ();

        yield return null;

    }

}