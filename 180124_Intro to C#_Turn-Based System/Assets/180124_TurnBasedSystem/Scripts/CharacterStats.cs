﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterStats {

    public float level;

    [HideInInspector] public bool dead;

    [Header ("Base Stats")]
    public float baseHP;
    public float baseStrength;

    [Header ("Level Multipliers")]
    public float strengthMultiply;
    public float healthMultiply;

    public CharacterStats(CharacterStats stats)
    {
        level = stats.level;
        dead = stats.dead;
        baseHP = stats.baseHP;
        baseStrength = stats.baseStrength;
        strengthMultiply = stats.strengthMultiply;
        healthMultiply = stats.healthMultiply;
    }

    public void LevelUp () //calculates level scaling and stats depending on class attributes
    {
        level ++;
        baseHP += (healthMultiply * level);
        baseStrength += (strengthMultiply * level);
    }

    public float CalculatedDamage(){
        //Add any buffs 
        return baseStrength;
    }

    //end of character attributes /\
}