﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBase : MonoBehaviour {
    public CharacterStats currentStats; //Modified working stats

    void Start () {
        (this as ICharacter).Init ();
        currentStats.dead = false;
        currentStats.LevelUp ();
    }

    [ContextMenu ("Level up this character")]
    public void LevelUp () {
        currentStats.LevelUp ();
    }

    public void Die () {
        BattleManager.instance.SearchAndRemove (this);
        Destroy (gameObject);
    }

}