﻿using UnityEngine;

[CreateAssetMenu(fileName = "CharacterObject", menuName = "Characters/CharacterObject", order = 0)]
public class CharacterObject : ScriptableObject {
    public CharacterStats _characterStats;                      //Creates characters as independant objects that the user can edit
}