﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour {

    public static BattleManager instance;

    public delegate void BattleEvents (CharacterBase newTarget);
    public static BattleEvents OnNewTargetSelected;

    public enum ActionManager {
        WAIT,
        DONE,
    }

    [System.Serializable] //allows script to be seen globally
    public class TeamCharacters {
        public List<CharacterBase> characters;
    }

    public ActionManager action;

    public List<TurnManager> ActionList = new List<TurnManager> (); //creates list of stages in a turn

    public List<TeamCharacters> CharacterList = new List<TeamCharacters> (); //creates list of characters in play
    public TeamCharacters activeTeam;

    public CharacterBase selectedTarget;

    public bool animating;

    void Awake () {
        instance = this;
    }

    void Start () {
        action = ActionManager.WAIT; //sets state to IDLE when game starts
        TurnManager.instance.BeginNewTurn ();
    }

    void Update () {
        switch (action) {
            case (ActionManager.WAIT):

                break;

            case (ActionManager.DONE):

               

                action = ActionManager.WAIT;
                break;
        }
    }

    public void UpdateTarget (CharacterBase newTarget) {
        selectedTarget = newTarget;
        if (OnNewTargetSelected != null) OnNewTargetSelected (selectedTarget);
    }

    public void SearchAndRemove (CharacterBase killedCharacter) {
        for (int i = 0; i < CharacterList.Count; i++) {
            for (int j = 0; j < CharacterList[i].characters.Count; j++) {
                if (killedCharacter == CharacterList[i].characters[j]) {
                    CharacterList[i].characters.RemoveAt(j);
                    j--;
                }
            }
        }
    }

  

    IEnumerator DealDamageWithAnim () {


        while (animating == true) {
            /*
             * 
             * Input animation here <-----------------
             *
             */
            yield return null;
        }
        action = ActionManager.DONE; //when turn is done, state is set to DONE so new turn can begin
    }

}