﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICharacter
{
    void Init();
    void MyTurn();
    void Damage(float amount);
}
