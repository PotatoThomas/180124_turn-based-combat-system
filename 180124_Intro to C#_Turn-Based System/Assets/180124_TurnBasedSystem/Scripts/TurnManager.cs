﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TurnManager : MonoBehaviour {

    public static TurnManager instance;

    public delegate void TurnEvents ();
    public static TurnEvents OnNewTurn; //Subscribe to this for turn based changes 
    public static TurnEvents OnNewRound; //Subscribe to this for round based changes (eg: poison damage)

    public List<int> turnOrder = new List<int> ();
    int currentTurnIndex = 0; //sets variable to determine turn order

    List<CharacterBase> activeTeam = new List<CharacterBase> ();
    public bool activeCharDone = false;

    void Awake () {
        Time.timeScale = 1;
        instance = this;
    }

    void Start () {
        SetTurnOrder (); //when game starts, turn order is set
    }

    public void TurnExecute () {
        activeTeam[0].GetComponent<ICharacter> ().MyTurn (); //starts active players turn
    }

    public void TurnComplete () {
        Debug.Log ("TURN COMPLETE");
        
        for (int i = 0; i < BattleManager.instance.CharacterList.Count; i++)
        {
            if (BattleManager.instance.CharacterList[i].characters.Count == 0){
                BattleManager.instance.CharacterList.RemoveAt(i);
                SetTurnOrder ();
                i--;
            }
        }


        if (BattleManager.instance.CharacterList.Count == 1) {
            Debug.Log ("GAME OVER");
            Time.timeScale = 0;
        }

        else
        {
            BeginNewTurn ();
        }

    }

    [ContextMenu ("New Order")]
    void SetTurnOrder () {
        turnOrder.Clear ();
        List<int> numbers = new List<int> ();
        for (int i = 0; i < BattleManager.instance.CharacterList.Count; i++) { //creates turn order for however many characters there are
            numbers.Add (i);
        }

        for (int i = 0; i < numbers.Count; i++) {
            int randomIndex = Random.Range (0, numbers.Count);
            turnOrder.Add (numbers[randomIndex]); //randomises the turn order
            numbers.RemoveAt (randomIndex);
            i--;
        }

        currentTurnIndex = 0;
    }

    [ContextMenu ("Next Turn")]
    public void BeginNewTurn () //When game starts or when a character is done with their turn, it chooses who goes next
    {
        //UIManager update to indicate new turn

        if (turnOrder.Count > 1 && BattleManager.instance.CharacterList[turnOrder[currentTurnIndex]].characters.Count > 0) {

            activeCharDone = false;
            Debug.Log ("Team " + turnOrder[currentTurnIndex]);
            CurrentPlayerTxt.instance.changePlayerName ("" + BattleManager.instance.CharacterList[turnOrder[currentTurnIndex]].characters[0].name); //Sends current Player Name to UI
            UIhealthtext.instance.changePlayerHP("" + BattleManager.instance.CharacterList[turnOrder[currentTurnIndex]].characters[0].currentStats.baseHP); //Sends current Health to UI
            UIdmgText.instance.changePlayerDMG("" + BattleManager.instance.CharacterList[turnOrder[currentTurnIndex]].characters[0].currentStats.baseStrength);  //Sends current Damage to UI
            activeTeam = null; //clears previous team from list
            activeTeam = BattleManager.instance.CharacterList[turnOrder[currentTurnIndex]].characters;
            BattleManager.instance.activeTeam = BattleManager.instance.CharacterList[turnOrder[currentTurnIndex]];

           

            IncreaseTurnIndex ();

        } else {
            if (BattleManager.instance.CharacterList.Count > 1) {
                IncreaseTurnIndex ();
                BeginNewTurn ();
            }

        }

        if (OnNewTurn != null) OnNewTurn ();

    }

    void IncreaseTurnIndex () {
        currentTurnIndex++; //sets next turn, represented by index number
        if (currentTurnIndex > BattleManager.instance.CharacterList.Count - 1) {
            Debug.Log ("End of the line");
            if (OnNewRound != null) OnNewRound ();
            currentTurnIndex = 0;
        } else {
            Debug.Log ("Still more teams");
        }
    }

 

}